import { Component, OnInit, ApplicationRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/providers/config/config.service';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/providers/loading/loading.service';
import * as moment from 'moment';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {

  myAccountData = {
    customers_firstname: '',
    customers_lastname: '',
    phone: '',
    password: '',
    customers_dob: '',
    dob:'',
    customers_id: '',
    image_id: 0,
    customers_gender:0,
    customer_phone:''
  };


  constructor(
    public config: ConfigService,
    public shared: SharedDataService,
    public loading: LoadingService) {
  }

  //============================================================================================  
  //function updating user information
  updateInfo() {
    if(this.myAccountData.customers_firstname == "" || this.myAccountData.customers_firstname == null || this.myAccountData.customers_firstname == undefined){
      this.shared.toast('First Name Cannot be empty');
      return;
    }
    if(this.myAccountData.customers_lastname == "" || this.myAccountData.customers_lastname == null || this.myAccountData.customers_lastname == undefined){
      this.shared.toast('Last Name Cannot be empty');
      return;
    }
    if(this.myAccountData.customer_phone == "" || this.myAccountData.customer_phone == null || this.myAccountData.customer_phone == undefined){
      this.shared.toast('Mobile Number Cannot be empty');
      return;
    }
    if(this.myAccountData.customer_phone.toString().length < 8){
      this.shared.toast('Mobile Number Length must be more than 8 characters');
      return;
    }   
     if(this.myAccountData.customer_phone.toString().length > 15){
      this.shared.toast('Mobile Number Length cannot be more than 15 characters');
      return;
    }
    if(this.myAccountData.password != ""){
      if(this.myAccountData.password.length < 8){
        this.shared.toast('Password Length must be more than 8 characters');
        return;
      }
    }
    // if(this.myAccountData.password == ''){
    //  this.myAccountData.password = 'none';
    // }
    this.loading.show();
    this.myAccountData.customers_dob = moment(this.myAccountData.dob).format('DD/MM/YYYY');
    this.myAccountData.phone = this.myAccountData.customer_phone.toString();
    this.myAccountData.customers_id = this.shared.customerData.customers_id;
    var dat = this.myAccountData;
    this.config.postHttp('updatecustomerinfo', dat).then((data: any) => {

      this.loading.hide();
      if (data.success == 1) {
        let d = data.data[0];
        console.log(data);
        this.shared.toast(data.message);;
        this.shared.login(d);
        this.myAccountData.password = "";
      }
      else {
        this.shared.toast(data.message);
      }
    }
      , error => {
        this.loading.hide();
        this.shared.toast("Error while Updating!");
      });
    }
  

  //============================================================================================

  ionViewWillEnter() {
    this.myAccountData.customers_firstname = this.shared.customerData.customers_firstname;
    this.myAccountData.customers_lastname = this.shared.customerData.customers_lastname;
    this.myAccountData.customer_phone = this.shared.customerData.phone;
    this.myAccountData.customers_gender = this.shared.customerData.gender;
    var dateString = this.shared.customerData.customers_dob; // Oct 23

    var dateParts:any = dateString.split("/");
    
    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
        this.myAccountData.dob = dateObject.toString(); 
    

  }

  ngOnInit() {
  }

}
